//
//  AppDelegate.swift
//  CarthageDemo
//
//  Copyright © 2018 Pulse. All rights reserved.
//

import UIKit

@UIApplicationMain

// MARK: UIApplicationDelegate
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        PulseSDK.sharedClient().delegate = self;

        return true
    }
}

// MARK: PulseSDKDelegate
extension AppDelegate: PulseSDKDelegate {
    
    func didReceiveDeepLinkNotification(_ notification: String?) {
        
    }
    
    func didReceivePreciseLocation(_ location: CLLocation?, error: String?) {
        
    }
    
    func didReceiveVenue(_ venue: [AnyHashable : Any]?, error: Error?, userLocation: CLLocation?, detectedBeacon: [AnyHashable : Any]?) {
        
    }
    
    func onLocalGeofenceCreated(_ gID: String?, success: Bool) {
        
    }
    
    func onLocalGeofenceDeleted(_ gID: String?, success: Bool) {
        
    }
    
    func onLocalGeofenceEntered(_ gID: String, type: LocalGeofence) {
        
    }
    
    func onLocalGeofenceExited(_ gID: String, type: LocalGeofence, dwell: Float) {
        
    }
}
