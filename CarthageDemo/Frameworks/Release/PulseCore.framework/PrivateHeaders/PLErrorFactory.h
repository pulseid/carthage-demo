//
//  PLErrorFactory.h
//  Core
//
//  Created by Pulse on 23/1/18.
//  Copyright © 2018 com.proximiti. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, PLErrorCode){
    PLURLError = 0,
    PLNetworkError,
    PLIGTimeoutError,
    PLIGAccuracyError,
    PLRegionUndetectedError,
    PLPredictionTimeoutError,
    PLPredictionError,
    PLAtVenueDisabled
};

@interface PLErrorFactory : NSObject

+ (NSError *) errorWithCode:(PLErrorCode)code userInfo:(NSDictionary*)userInfo;

@end
