//  Created by Pulse on 18/12/2014.
//  Copyright (c) 2017 com.pulse. All rights reserved.


#import <Foundation/Foundation.h>
#import <PulseCore/PulseRegion.h>
@import CoreLocation;

/**
 Denotes types of LocationPermission.
 */
typedef NS_ENUM(NSUInteger, LocationPermission){
    PL_LOCATION_UNKNOWN = 0,
    PL_LOCATION_ALWAYS = 1,
    PL_LOCATION_WHENINUSE = 2
};

@protocol PulseLocationManagerDelegate;

@interface PulseLocationManager : NSObject<CLLocationManagerDelegate>

+ (instancetype)sharedClient;
- (void)registerRegion:(PulseRegion*)pulseRegion;
- (void)enablePreciseLocation;
- (NSSet*)currentMonitoredRegions;
- (void)startSignificantLocationUpdate;
- (void)stopSignificantLocationUpdate;
- (void)stopMonitoringRegions:(NSString*)regionType;
- (void)stopGeoRegionsExcept:(NSArray*)array;
- (void)triggerGeofenceExit;
- (LocationPermission)locationPermission;
/**
 Check whether a beacon region is already monitored.
 @param regionID identifer for the region in full name. i.e PulseBeacon~UUID~Major~Minor~.
 @return return YES if it is monitored.
 */
- (BOOL)isBeaconMonitoredForExit:(NSString*)regionID;
- (NSString*)getUserCurrentLocation;
- (void)stopPreciseLocation;
- (int)numberOfRegionsBeingMonitored:(NSString*)type;
- (CLLocation*)getCurrentLocationObj;
+ (NSString*)getFormattedLocation:(CLLocation*)location;

@property (nonatomic, strong) NSThread *thread;
@property (nonatomic) BOOL ranging;
@property (nonatomic,strong) CLLocationManager *locationManager;
@property (nonatomic,strong) NSString* preciseLocation;
@property (nonatomic, weak, nullable) id<PulseLocationManagerDelegate> delegate;

@end

@protocol PulseLocationManagerDelegate <NSObject>

@required
- (void) pulseLocationManager:(nonnull PulseLocationManager *)manager startMonitoringIGRegion:(nonnull PulseRegion *)region;
- (void) pulseLocationManager:(nonnull PulseLocationManager *)manager stopMonitoringIGRegion:(nonnull NSString *)identifier;
@optional
- (void) pulseLocationManager:(nonnull PulseLocationManager *)manager didFailWithError:(nullable NSError *)error;
- (void) pulseLocationManager:(nonnull PulseLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status;

@end

