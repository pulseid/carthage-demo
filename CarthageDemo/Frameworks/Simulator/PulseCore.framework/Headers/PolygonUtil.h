//
//  PolygonUtil.h
//
//  Created by Pulse on 14/3/18.
//  Copyright © 2018 com.pulse. All rights reserved.
//

#import <Foundation/Foundation.h>

@import CoreLocation;

/**
 {
 "polygon": {
    "verticles": [
        [1,2],
        [3,4],
        [5,6],
        [1,2]
        ]
    }
 }
 */

@interface Polygon : NSObject

@property (strong, nonatomic) NSArray<NSArray<NSNumber*> *> *verticles;

@end

@interface PolygonUtil : NSObject

struct SphericalVector{
    double rho;
    double theta;
    double phi;
};

typedef struct SphericalVector SphericalVector;

struct CarthesianVector{
    double x;
    double y;
    double z;
};
typedef struct CarthesianVector CarthesianVector;

+ (SphericalVector) sphericalVectorFromCoordinates:(CLLocationCoordinate2D) coord;

+ (CarthesianVector) carthesianVectorFromSphericalVector:(SphericalVector) sphericalVector;

+ (BOOL) isWithinPolygon:(Polygon *)polygon ForCoordinate:(CLLocationCoordinate2D) coord;

@end


