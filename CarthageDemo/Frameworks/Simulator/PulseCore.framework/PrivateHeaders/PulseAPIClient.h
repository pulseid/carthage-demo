//
//  PulseAPIClient.h
//  Core
//
//  Created by Pulse on 2018/2/3.
//  Copyright © 2018 com.pulse. All rights reserved.
//

/**
 * Pulse API Client is a private class that enables modules to interact with API.
 */

#import <Foundation/Foundation.h>
#import <PulseCore/PulseRegion.h>
#import <PulseCore/IGConfigModel.h>
@interface PulseAPIClient : NSObject

+ (void)saveLocation:(CLLocation*)location;
+ (void)setGeofenceHitTestProcess:(BOOL)running;
+ (void)setBeaconProcess:(BOOL)running;
+ (void)setIGProcess:(BOOL)running;
+ (IGConfigModel *)getIGConfig;
+ (BOOL)isBeaconOnSite:(NSString*)identifier;
@end
