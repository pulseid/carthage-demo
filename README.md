## Steps to run this project:
- Run `carthage update --platform iOS`


## Steps when creating a new project
- Create a Cartfile and add the following dependencies:

````
github "jsonmodel/jsonmodel"

github "yourkarma/JWT" "master"

github "kciter/Floaty" ~> 3.0.0

github "CocoaLumberjack/CocoaLumberjack"

github "pulse-id/ReactiveObjC"

````

- Run `carthage update --platform iOS`
- Open `.xcodeproj` and add the following frameworks from `Carthage/Build/iOS`

````
Base64.framework

CocoaLumberjack.framework

Floaty.framework

JSONModel.framework

JWT.framework

PulseReactiveC.framework```

- Also add the following frameworks from this project:
````
PulseCore.framework
PulseIG.framework```
- Select your target in Xcode and add the above frameworks to the `Embedded Binaries`


## NOTE: Tested with Xcode version `9.4.1 (9F2000)`

